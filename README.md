# Author
## Kyra Novitzky

## Contact Address
knovitzk@uoregon.edu

## Description
Essentially replacing the calculator here (https://rusa.org/octime_acp.html). We can also use that calculator to clarify requirements and develop test data.
Each time a specified distance is filled in on the web browser, Ajax will respond by filling in the opening and closing times.
Additions:
Simple list of controle times from project 4 stored in MongoDB database.
1)Creation of two buttons ("Submit") and ("Display") in the page where have controle times.
2)On clicking the Submit button, the control times should be entered into the database.
3)On clicking the Display button, the entries from the database should be displayed in a new page.

## Test Cases
1)When new data is submitted and displayed through the "submit" and "display" buttons found on the homepage, the data from
previous submissions does not appear on the new display page.
2)When no data is submitted and the user continues to press the "display" button, no new page will appear because there is
no data to display.
3)Even if open times and close times are displaying on the homepage, if the data is not submitted, the "display" button will
not work until it is.

### Credits
Credits to Michal Young for the initial version of this code.