import os
import flask
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations

app = Flask(__name__)

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb

@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Calc_time JSON request")
    km = request.args.get('km', 999, type=float)
    begin_datetime = request.args.get('begin_date', "2017-01-01", type=str) + ' ' + request.args.get('begin_time', "00:00", type=str) + '-08:00'
    begin_arrow = arrow.get(begin_datetime, "YYYY-MM-DD hh:mmZZ")
    brevets_distance = request.args.get('brevet_dist_km', 200, type=float)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    open_time = acp_times.open_time(km, brevets_distance, begin_arrow.isoformat())
    close_time = acp_times.close_time(km, brevets_distance, begin_arrow.isoformat())
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)

@app.route("/_submit_to_db")
def _submit_to_db():
    app.logger.debug("Submitting JSON request")
    km = request.args.get('km', type=float)
    open_time = request.args.get('open', type=str)
    close_time = request.args.get('close', type=str)
    item_doc = {
        'km': km,
        'open_time': open_time,
        'close_time': close_time
    }
    db.tododb.insert_one(item_doc)
    app.logger.debug("Inserted km={}, open={}, close={}".format(km, open_time,close_time))
    return flask.jsonify()

@app.route("/_display")
def _display():
    app.logger.debug("Data being displayed")
    _items = db.tododb.find()
    items = [item for item in _items]
    if(len(items) == 0):
        return flask.render_template('error.html')
    for item in items:
        item.pop("_id")
    app.logger.debug(items)
    result = {"items":items}
    db.tododb.remove({})
    return flask.jsonify(result=result)


if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)

